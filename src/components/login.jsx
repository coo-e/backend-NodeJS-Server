var React = require('react/addons');

// bind the action creator to the action variable, so when the action is called it is dispatched and sent to update queries in stores
var action = require('./../actions/userActionCreator.jsx');

//ecrypts and decrypts information using salts and hashing
var bcrypt = require('bcryptjs');

//this is a simple helper file that allows us to make ajax calls without repetition
var helper = require('./../helpers/RestHelper.js');

//returns a component
var login = React.createClass({
	getInitialState:function(){
		return {loginUser: '',
				registerUser: '',
				loginPassword: '',
				registerPassword: '',
				loggedIn: false,
				failLogin: false,
				failRegister: false,
				successRegister: false,
				loggedInUser: '' }
	},
	//updates login username with input
	handleLoginUsername: function(e){
 				this.setState({loginUser: e.target.value});
	},
	//updates register username with input
	handleRegisterUsername: function(e){
 				this.setState({registerUser: e.target.value});
	},
	//updates login password with input
	handleLoginPassword: function(e){
 				this.setState({loginPassword: e.target.value});
	},
	//updates register password with input
	handleRegisterPassword: function(e){
		// console.log(e);
				this.setState({registerPassword: e.target.value});
	},
	//registers a new user
	register : function(e){
		e.preventDefault();
		console.log('registered')

		//IMPORTANT -- initialize this in order to use it outside of scope
		var that = this;

		//get all of the users
		helper.get("api/users")
		//upon success
		 	.then(function(data){
		 	var users = data;

			try{
				//check to see if a user exists with same username
				var _user = users.filter(function(a){ return a.username == that.state.registerUser})[0];
				
				//if one exists, set failure and success states
				//clear out inputs
				if (typeof _user != 'undefined'){
					that.setState({failRegister: true,
									failLogin: false,
									successRegister: false});
					that.setState({registerUser: '',
								registerPassword: ''})
				}

				//if registerable, success register is true and register the user
				else{
				that.setState({failRegister: false,
									failLogin: false,
								successRegister: true});
				action.register({username: that.state.registerUser,
								password: that.state.registerPassword});
				that.setState({registerUser: '',
								registerPassword: ''})
			}

			
				}
			catch(e){

				console.log(e)
			}
		});


	},
	//log user in
	login : function(e){
		e.preventDefault();

		//create 
		// var pword = this.state.loginPassword;
		// var uname = this.state.loginUser;
		var logged = false;
		var that = this;
			

			helper.get("api/users")
		 	.then(function(data){
		 		
		 		var users = data;
		 		console.log(data);
		 		// triggerListeners();
		 	
		 	console.log(users);
		 	try{
		 		// check to see if the user exists in the database, if false, return failed state info and quit
		var _user = users.filter(function(a){ return a.username == that.state.loggedInUser})[0];
		// console.log(_user);
		// console.log(uname);
		// console.log(pword);
		// console.log(_user.password);
		}
	catch(e){
		return that.setState({
			failLogin: true,
			successRegister: false,
			failRegister: false
		})
	}

		//check to see if password matches
		//first argument is the password passed, second is the stored password
		 bcrypt.compare(that.state.loginPassword, _user.password, function(err, isMatch) {
		        if (err) console.log('failed login');
		        //if it is a match, logged gets true and states get updated
		        if(isMatch){
		        	console.log('success');
		        	logged = true;
		        	console.log(logged);

		        	if(logged){
		        		//update logged in with true, and pass the logged in users username
		 				that.setState({loggedIn: logged,
		 								loggedInUser: _user.username})
		 			};
		        }
		        else
		        {
		        	//if its not a match, return failed login
		        	that.setState({
						failLogin: true,
						successRegister: false,
						failRegister: false
					});
		     		console.log('fail');   	
		        }
		    });

});

		//clear user fields
		this.setState({loginUser: '',
						loginPassword: ''});

	},

	logout:function(){
		//changed states to false and cleared
		this.setState({loggedIn : false,
						failLogin: false,
						failRegister: false,
						successRegister: false,
						loggedInUser: ''});



	},

	//render function is the only required function ina component and it returns html, with some jsx magic in the mix
	render: function(){
		var logout  = <div ><h1>Welcome, {this.state.loggedInUser}</h1>
						<button onClick={this.logout}>logout</button>
						</div> ;


		return(
			<div>
			//if logged in is false, then display login/register page otherwise display logout page
			{ !this.state.loggedIn ? 
				<div>
				//if failLogin , display and so on
				{this.state.failLogin ? <div> Failed to provide correct login information </div> : null}
				{this.state.failRegister ? <div> User has already been taken </div> : null}
				{this.state.successRegister ?  <div>Successfull Registration!</div> : null}

				//on submit is called with the button inside the form, calls this.register function from above
				<form onSubmit={this.register} className="three columns" >
					<h2>Register</h2>
					//everytime change listeneres are trigger, run handler and states are updated
					<input value={this.state.registerUser} onChange={this.handleRegisterUsername} type="text" placeholder="username" />
					<input value={this.state.registerPassword} onChange={this.handleRegisterPassword} type="text" placeholder="password" />
					<button>Register</button>
				</form>
				<form  onSubmit={this.login} className="three columns" >
					<h2>Login</h2>
					<input value={this.state.loginUser} onChange={this.handleLoginUsername} type="text" placeholder="username" />
					<input value={this.state.loginPassword} onChange={this.handleLoginPassword} type="text" placeholder="password" />
					<button>Login</button>
				</form>
				</div>
			 : null
		}
				//here is the same idea, but a variable of jsx is passed instead of directly. variable logout is found at beginning of render function
				//the if statement in jsx is, test, question mark seperator, if true display, else display
				{ this.state.loggedIn ? logout : null}

			</div>

			)
	}
});

module.exports = login;