"use strict";
/**
 * Created by kingHenry on 2/9/16.
 */
class ChatRoom {
    constructor(exchange){
        this._exchange=exchange;
    }
    get exchange(){
        return this._exchange;
    }

    toString() {
        return `${this.exchange}`
    }

    on_connect() {
        output.innerHTML += 'Connected to RabbitMQ-Web-Stomp<br />';
    console.log(client);
    client.subscribe(mq_queue, this.on_message);
    }

// This will be called upon a connection error
    on_connect_error() {
    output.innerHTML += 'Connection failed!<br />';
    }

// This will be called upon arrival of a message
    on_message(m) {
    console.log('message received');
    var jsonMsg = JSON.parse(m);
    console.log("+++++AAAAA");
    console.log(jsonMsg.chatBody);
    output.innerHTML += jsonMsg.chatBody.user + ':'+ jsonMsg.chatBody.messageBody + '<br />';
    }

}

module.exports=ChatRoom;
