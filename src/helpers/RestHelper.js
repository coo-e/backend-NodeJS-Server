//this, to my knowledge, is not a requirement however allows for easy access to Ajax calls throughout the application
//These calls are static with all get, post, patch and delete functions so having one place makes them DRY and modular


var $ = require('jquery'); // require jquery, installed via npm

var ajaxCalls =  {
	//get returns information from the passed url, ajax then sends a function and callback promise and passes the necessary ajax information
	get: function(url){

		//a promise is essentially a callback function that binds event handlers
		//promise keyword simply means run when the call has finished however available functions include
		//.done().fail().always() which are called when call succeeds, fails or regardless in always' case
		return new Promise(function(success, error){
			$.ajax({
				url: url,
				//passes json to know how to parse the response,
				// different than others because the callback includes parsable info instead of an update to the database
				dataType: "json",
				success: success,
				error: error
			})
		})
	},
	post: function(url, data){
		//data in this case would be the information being passes to the database
		return new Promise(function(success, error){
			$.ajax({
				url: url,
				type: "POST",
				success: success,
				error: error,
				data: data
			})
		})
	},
	// a patch is similar to put in that it updates an entry, however, 
	//in patches case, it updates parts of the resource instead of updating with a replacement
	patch: function(url, data){
		return new Promise(function(success, error){
			$.ajax({
				url: url,
				type: "PATCH",
				success: success,
				error: error,
				data: data
			})
		})
	},
	//shortened to del due to delete existing as a keyword
	del: function(url){
		return new Promise(function(success, error){
			$.ajax({
				url: url,
				type: "DELETE",
				success: success,
				error: error
			})
		})
	}
}

//module exports the object to the location from which u require it passing all the available methods from within the object
module.exports = ajaxCalls;