/**
 * Created by kingHenry on 2/11/16.
 */

import { bootstrap } from "angular2/platform/browser";
import { Component } from "angular2/core";
import { NgFor } from "angular2/common";

@Component({
    selector: 'angular2Test',
    template: `
  <ul>
    <li *ngFor="#name of names">Hello {{ name }}</li>
  </ul>
`
})
class HelloWorld {
    names: string[];
    constructor() {
        this.names = ['Ari', 'Carlos', 'Felipe', 'Nate'];
    }
}
bootstrap(HelloWorld);