var dispatcher = require('./../dispatcher.js');

module.exports = {
	login: function(username){
		dispatcher.dispatch({
			payload:username,
			type: "user:login"
		});
	},
	register:function(username){
		dispatcher.dispatch({
			payload:username,
			type: "user:register"
		})
	},
	unbuy: function(item){
		dispatcher.dispatch({
			payload:item,
			type: "grocery-item:unbuy"
		})
	},
	buy: function(item){
		dispatcher.dispatch({
			payload:item,
			type: "grocery-item:buy"
		})
	}

}
