var dispatcher = require('./../dispatcher.js');

//this is a library of helper methods for store
//this creates action objects using new fields of data and the actiontype
//these are then passed to the dispatcher to update the propogate the stores
//best representation of dispatcher and actions are here https://facebook.github.io/react/blog/2014/07/30/flux-actions-and-the-dispatcher.html

// DATA FLOW
//this is the first point of contact with user interaction
//action called here and sent to dispatcher
//dispatcher invokes callbacks set by the store
//store changes events , queries and data
//updates components and views, which u can then invoke another action from



module.exports = {
	login: function(user){
		dispatcher.dispatch({
			payload:user,
			type: "user:login"
		});
	},
	register:function(user){
		dispatcher.dispatch({
			payload:user,
			type: "user:register"
		})
	}
	// unbuy: function(item){
	// 	dispatcher.dispatch({
	// 		payload:item,
	// 		type: "grocery-item:unbuy"
	// 	})
	// },
	// buy: function(item){
	// 	dispatcher.dispatch({
	// 		payload:item,
	// 		type: "grocery-item:buy"
	// 	})
	// }

}
