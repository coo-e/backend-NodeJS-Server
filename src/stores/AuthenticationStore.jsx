//calls dispatcher in order to handle data queries
var dispatcher = require('./../dispatcher.js');

//helper is just a means to keep ajax calls dry
var helper = require('./../helpers/RestHelper.js');


function AuthenticationStore(){
	// intialize our users
	var users = [];

	//populate our users using ajax helper get on the collection at api/users and triggering update listenerss
	helper.get("api/users")
 	.then(function(data){
 		users = data;
 		triggerListeners();
 	})


 	//intialize listeners
	var listeners = [];

	//tricky, this is called at the end of the store as a return, a means of instantiating and passing data in a users: field
	function getUsers(username){
		return users;
	}

	//pushn users onto users array, trigger changelisteners, use ajax helper to post new user to collection
	function addNewUser(username, password){

		if(!username =='' && !password ==''){
		users.push({
			username: username,
			password : password
		});


		
		triggerListeners();

			helper.post("/signup", {
			username: username,
			password: password
		});
		}
		else{
			//otherwise suck my dick
			console.log('smd');
		}

	}
	
	//return from store, populates store slisteners for dispatcher
	function onChange(listener){
		listeners.push(listener);
	}

	//trigger dispatch listeners
	function  triggerListeners(){
		listeners.forEach(function(listener){
			listener(users);
		})
	};

	//this is where actions are passed from the component, to store,  to the dispatcher, and the dispatched back to store to populate
	dispatcher.register(function(event){
		var split = event.type.split(':');
		if(split[0]==='user'){
			switch(split[1]){
				//does nothing currently, should probably handle the auth here, instead of in login.jsx
				case "login":
					break;
				case "register":
					//login calls register action, action dispatches the payload,  
					addNewUser(event.payload.username, event.payload.password);
					console.log('register  from authstore');
					break;
			}
		}
	});

	return{
		getUsers: getUsers,
		onChange: onChange
	}
}

module.exports = new AuthenticationStore();