//package allows u to create globally unique identifiers
var guid = require('guid');


var listeners = {};

//best explanation of the dispatcher and action creators can be found here https://facebook.github.io/react/blog/2014/07/30/flux-actions-and-the-dispatcher.html

//dispatcher acts as the central hub of dataflow in the flux architecture
//its a registry of callbacks and can be invoked in order
//the order is important as the application grows larger, dispatch will update stores synchronously since stores can depend on one another

//each store registers a callback with the dispatcher thus the register method
//it is then disaptched propogating data to all of the stores using only a data payload argument

//not sure what listeneres are exactly at the moment but they relate to the synchronous nature of the store updates
//these listeners are specific to the dispatch method and are neglibible at this point
module.exports = {
	register: function(cb){
		//creates a unique id
		var id = guid.raw();
		listeners[id] = cb;
		return id;
	},
	dispatch: function(payload){
		console.info("Dispatching ...", payload);
		for(var id in listeners){
			var listener = listeners[id];
			listener(payload);
		}
	}
}