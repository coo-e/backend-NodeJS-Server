
var React = require('react'),
    ReactDOM = require('react-dom'),
    _ = require('lodash'),
    d3 = require('d3'),
    Controls=require('./controls.jsx'),
    drawers=require('./drawers.jsx'),
    queue = require('queue-async'),
    meta = require('./meta.jsx');


var H1BGraph = React.createClass(
    {
        componentWillMount: function(){
            this.loadRawData();
        },
        getInitialState:function(){
            return {rawData: [],
                dataFilter: function () { return true; }};
        },

        updateDataFilter: function (filter) {
            this.setState({dataFilter: filter});
        },
        loadRawData:function(){
            var dateFormat = d3.time.format("%m/%d/%y");
            d3.csv(this.props.url)
                .row(function(d){
                    if
                    (!d["Time to ED"]){
                        return null;
                    }else if(d.Acuity_initial==99999){
                        return null
                    }else{
                    return {acuityInitial: Number(d.Acuity_initial),
                        hour: Number(d["Time to ED"]),
                        waitTime: Number(d.HOUR)}
                }}.bind(this))
                .get(function (error,rows){
                    if (error){
                        console.error(error);
                        console.error(error.stack);
                    }else {
                        this.setState({rawData:rows});
                    }
                }.bind(this));
        },
        render:function(){
            if (!this.state.rawData.length) {
                return (
                    <h2>Loading Patient Wait times vs Acuity Types histogram..
                    </h2>
                );
            }

            var params = {
                    bins: 24,
                    width: 500,
                    height: 500,
                    axisMargin: 83,
                    topMargin: 10,
                    bottomMargin: 5,
                counts:[0,1000],
                    value: function (d) { return d.waitTime; }
                },
                fullWidth = 700;
            var filteredData = this.state.rawData.filter(this.state.dataFilter);
            return (
                <div>
                    <meta.Title data={filteredData}/>
                    <meta.Description data={filteredData} allData={this.state.rawData} />
                    <div className="row">
                        <div className="col-md-12">
                            <svg width={fullWidth} height={params.height}>
                                <drawers.Histogram {...params} data={filteredData}/>
                            </svg>
                        </div>

                    </div>
                    <Controls data={this.state.rawData} updateDataFilter={this.updateDataFilter}/>
                </div>
            );
        }
    }
);


ReactDOM.render(<H1BGraph url="data/acuityWait.csv"/>, document.querySelectorAll('#h1bgraph')[0]);