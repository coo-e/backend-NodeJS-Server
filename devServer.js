/**
 * Created by kingHenry on 12/6/15.
 */
var path = require('path');
var express = require('express');
var webpack = require('webpack');
var config = require('./webpack.config.dev');
var routes = require('./routes/index');
var app = express();

var passport = require('passport');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var compiler = webpack(config);
var r = require('rethinkdb');
var rethinkConfig = require('./app/config.js');
var jwt = require('express-jwt');
var session = require('express-session');
var bcrypt=require('bcrypt-nodejs');
var flash    = require('connect-flash');
var User   = require('./app/models/users').Users; // get our thinky model

require('./config/passport')(passport);

var api = require('./routes/api')(passport);
function handleError(res, error) {
    return res.send(500, {error: error.message});
}
function createConnection(req, res, next) {
    r.connect(rethinkConfig.rethinkdb, function(error, conn) {
        if (error) {
            handleError(res, error);
        }
        else {
            // Save the connection in `req`
            req._rdbConn = conn;
            // Pass the current request to the next middleware
            next();
        }
    });
}

app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.urlencoded({ extended: true }));    // parse application/x-www-form-urlencoded
app.use(bodyParser.json());    // parse application/json
app.use(logger('dev'));

// routes ======================================================================


app.use(session({ secret: 'someshittysecret' ,cookie: { maxAge: 60000 },resave: true,
    saveUninitialized: true}));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash());
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport
app.use(createConnection);
app.use('/', routes);
app.use('/api', api);
app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));
app.use(express.static('public'));


app.get('/', function(req, res) {
    res.render('fixedHeader.jade', { title: 'Express' });
    //res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/deep', function(req, res) {
    var password = bcrypt.hashSync('password');
    // create a sample user
    var deep = new User({
        email: 'deep@taco.com',
        password: 'password',
        admin: true
    });

    // save the sample user
    deep.save().then(function(deep) {
        console.log(deep);
        res.json({ success: true });
    });
});

app.get('/retrieve/matches/:email/:id',function(req, res, next) {
    var userId=Number(req.params.id);
    var userEmail=req.params.email;
    r.table('matchdata')
        .filter({email:userEmail,participantId:userId})
        .run(req._rdbConn, function(error, cursor) {
        if (error) {
            handleError(res, error);
            next();
        }
        else {
            // Retrieve all the results
            cursor.toArray(function(error, result) {
                if (error) {
                    handleError(res, error);
                }
                else {
                    // Send back the data as json
                    res.send(JSON.stringify({"matchList":result}));
                }
            });
        }
    });
});


app.listen(3000, '0.0.0.0', function(err) {
    if (err) {
        console.log(err);
        return;
    }

    console.log('Listening at dev.telluswho.com:3000 if vagrant, otherwise localhost:3000');
});