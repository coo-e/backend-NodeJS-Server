#!/usr/bin/env bash

#variables
sbtVersion="0.13.9"

echo "=========================================="
echo "Provision VM START"
echo "=========================================="

sudo apt-get update

###############################################
# install prerequisits
###############################################
sudo apt-get -y -q upgrade
sudo apt-get -y -q update
sudo apt-get -y -q install software-properties-common htop
sudo apt-get -y -q install build-essential
sudo apt-get -y -q install tcl8.5

###############################################
# Install Git
###############################################
sudo apt-get -y install git

###############################################
# Install imagemagick
###############################################
sudo apt-get -y install imagemagick
###############################################
# Install Unzip
###############################################
sudo apt-get -y install unzip
sudo apt-get install -q -y curl wget

###############################################
# Install NodeJS
###############################################
curl --silent --location https://deb.nodesource.com/setup_4.x | sudo bash -
sudo apt-get -y install nodejs
ln -s /usr/bin/nodejs /user/bin/node
# Add node_modules to environment variables
echo "export NODE_PATH=/usr/local/lib/node_modules" >> ~/.bashrc

###############################################
# Install NPM
###############################################
sudo apt-get -y install npm

###############################################
# Install Grunt
###############################################
sudo npm install -g grunt grunt-cli

###############################################
# Install Bower
###############################################
sudo npm install -g bower

###############################################
# Install RabbitMQ
###############################################
cat >> /etc/apt/sources.list <<EOT
deb http://www.rabbitmq.com/debian/ testing main
EOT

sudo wget http://www.rabbitmq.com/rabbitmq-signing-key-public.asc
sudo apt-key add rabbitmq-signing-key-public.asc

sudo apt-get update


sudo apt-get install -q -y rabbitmq-server

# RabbitMQ Plugins
service rabbitmq-server stop
rabbitmq-plugins enable rabbitmq_management
rabbitmq-plugins enable rabbitmq_jsonrpc
rabbitmq-plugins enable rabbitmq_stomp
rabbitmqctl
service rabbitmq-server start

rabbitmq-plugins list

###############################################
# Reset bash
###############################################
source ~/.bashrc


###############################################
# Show installation summary
###############################################
echo "=========================================="
echo "Provision VM summary"
echo "=========================================="
echo "Dependencies installed:"
echo " "
echo "NodeJS version:"
node -v
echo " "
echo "NPM version"
npm -v
echo " "
echo " "
echo "Bower version:"
bower -v
echo " "
echo "=========================================="
echo "Provision VM finished"
echo "=========================================="
