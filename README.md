*There is  Vagrantfile for building a virtual machine to run this application**
=================================================================================


REQUIREMENTS:
-------------
* at least 4GB of ram (preferably >=6)
* at least a quad-core machine (2 cores allocated to the VM that will be spawned)
Download virtualBox in order for vagrant to work:

[Virtual Box Downloads page](https://www.virtualbox.org/wiki/Downloads)

Then you can install vagrant:

[Vagrant download page](https://www.vagrantup.com/downloads.html)

Install both for your respective platform (windows, osx, etc)

To set the hosts accordingly in order to get oAuth working install the hosts plugin for vagrant:

[Vagrant-Hostsupdater](https://github.com/cogitatio/vagrant-hostsupdater)

To Install:

 `vagrant plugin install vagrant-hostsupdater`


Then you will be able to run `vagrant up --provider=virtualbox`

If you are using VMWare Fusion you can run `vagrant up --provider=vmware_fusion`



*Note*

`/react-webpack-starter/` is a mounted folder. This means that if you edit any files on your local box, virtual box
will reload them automatically. You do not have to `scp` into the VM in order to upload updated files.

Your folder `react-webpack-starter/` on local will always have the same contents as `/vagrant/` on the virtual machine
that has been created.

Sometimes there are issues with source-ing the .bashrc file, so just in case it never hurts to run

`source ~/.bashrc` one more time just for good measure to ensure all symlinks/env variables are in place and active.

Make sure to install all required NPM libraries, by running `npm install`

To run the app run `npm start`

It should be visible in your browser at [dev.telluswho.com:3000](http://dev.telluswho.com:3000)