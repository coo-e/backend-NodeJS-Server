
// Use SockJS
Stomp.WebSocketClass = SockJS;

//// Connection parameters
//var mq_username = "telluswho",
//    mq_password = "telluswho",
//    mq_vhost    = "/",
//    mq_url      = 'http://telluswho.such.works:15674/stomp',
//
//// The exchange we will read. Client will receive
//// all messages published in the exchange, no routing key given bc
////fanout. Should subclass this file so we can pass the
//// particular exchange as an argument {soccer@newark}
//    mq_queue    = "/exchange/"+#{chatExchange};
//    console.log({#chatExchange});

// This is where we print incomoing messages
var output;

// This will be called upon successful connection
function on_connect() {
    output.innerHTML += 'Connected to RabbitMQ-Web-Stomp<br />';
    console.log(client);
    client.subscribe(mq_queue, on_message);
}

// This will be called upon a connection error
function on_connect_error() {
    output.innerHTML += 'Connection failed!<br />';
}

// This will be called upon arrival of a message
function on_message(m) {
    console.log('message received');
    var jsonMsg = JSON.parse(m);
    console.log("+++++AAAAA");
    console.log(jsonMsg.chatBody);
    output.innerHTML += jsonMsg.chatBody.user + ':'+ jsonMsg.chatBody.messageBody + '<br />';
}

// Create a client
var client = Stomp.client(mq_url);

window.onload = function () {
    // Fetch output panel
    output = document.getElementById("output");

    // Connect
    client.connect(
        mq_username,
        mq_password,
        on_connect,
        on_connect_error,
        mq_vhost
    );
}