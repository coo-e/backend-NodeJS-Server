/**
 * Created by kingHenry on 1/27/16.
 */
/*
 * @Routing file: chatRouter.js
 * This is the controller to handle all request related AMQP broker e.g. publish messages or connect to AMQP broker
 */


var amqp = require('amqplib/callback_api');
var amqpConn = null; // connection object from AMQP to create channel
var pubChannel = null; // Channel object for AMQP
var offlinePubQueue = []; // Holds non-published messages
/*var express = require('express');
 var bodyParser = require('body-parser');
 var app = express();
 app.use(bodyParser.json());
 app.use(bodyParser.urlencoded({ extended: false }));*/


/*
 * @Description This function is called from client to publish new message
 */
function publishMessage(req, res){
    /* Temp Code to generate client json request*/
    var reqData = null;
    var exchangeName = req.body.exchangeName;
    var messageBody = req.body.message;
    var activity=req.body.activity;
    var userName=req.body.userName;

    reqData={chatBody:{
        exchange: exchangeName,
        messageBody: messageBody,
        activity:activity,
        user:userName
    }};
    var reqDataString = JSON.stringify(reqData);
    console.log(JSON.stringify(reqData));
    var jsonReq = JSON.parse(reqDataString);
    /*Temp code End Here*/
    try {
        console.log("Request received to publish message");
        //var jsonReq = JSON.parse(req.body);
        console.log("Request " + jsonReq);
        console.log("Exchange: " + jsonReq.chatBody.exchange + " MessageBody: " + jsonReq.chatBody.messageBody);
        res.end();
        publish(jsonReq.chatBody.exchange, '', new Buffer(reqDataString));
    }catch(e){
        console.log("Error while publishing a message to [AMQP]");
        console.log(e.message);
    }

    try {
        while (true) {
            console.log("Check if there are any non published message in offline Queue");
            var m = offlinePubQueue.shift();
            if (!m) break;
            console.log("Found non published message in offlineQueue @exchange: " + m[0]);
            publish(m[0], m[1], m[2]);
        }
    }catch(e){
        console.log("error while publishing a messages from offlinePubQueue");
        console.log(e.message);
    }
}

/*
 * @Description connect AMQP broker and open new connection (Connection factory)
 * This function is called when server starts up at first
 */

function start() {
    console.log("Initiating [AMQP] connection");
    amqp.connect("amqp://telluswho:telluswho@45.55.60.150:5672" + "?heartbeat=60", function(err, conn) {
        if (err) {
            console.error("[AMQP]", err.message);
            return setTimeout(start, 1000);
        }
        conn.on("error", function(err) {
            if (err.message !== "Connection closing") {
                console.error("[AMQP] conn error", err.message);
            }
        });
        conn.on("close", function() {
            console.error("[AMQP] reconnecting");
            return setTimeout(start, 1000);
        });
        console.log("[AMQP] connected");
        amqpConn = conn;
        startPublisher();
    });
}

/*
 * @Description Open a single channel from AMQP connection object (amqpConn)
 * This function is called from start() function
 */
function startPublisher() {
    console.log("Opening new channel from connection object");
    amqpConn.createConfirmChannel(function(err, ch) {
        if (closeOnErr(err)) return;
        ch.on("error", function(err) {
            console.error("[AMQP] channel error", err.message);
        });
        ch.on("close", function() {
            console.log("[AMQP] channel closed");
        });
        pubChannel = ch;
        console.log("New channel is opened");
    });
}

/*
 * @Description this is the actual function that publish messages to AMQP broker.
 * By Default it uses fanout strategy.
 * IF message isn't published successfully, it will be stored into offlinePubQueue
 */
function publish(exchange, routingKey, content) {
    try {
        console.log("configuring channel to publish message");
        pubChannel.assertExchange(exchange, 'fanout',{durable:true});
        pubChannel.publish(exchange, routingKey, content, { persistent: true },
            function(err, ok) {
                if (err) {
                    console.error("[AMQP] publish", err);
                    offlinePubQueue.push([exchange, routingKey, content]);
                    pubChannel.connection.close();
                }

            });
    } catch (e) {
        console.error("[AMQP] publish", e.message);
        offlinePubQueue.push([exchange, routingKey, content]);
    }

}


function closeOnErr(err) {
    if (!err) return false;
    console.error("[AMQP] error", err);
    amqpConn.close();
    return true;
}


exports.publishMessage = publishMessage;
start();