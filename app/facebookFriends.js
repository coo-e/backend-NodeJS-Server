/**
 * Created by kingHenry on 1/26/16.
 */
var request = require('request');


/*
structure of facebook graph API request URL to retrieve friends

'https://graph.facebook.com/v2.4/{userId}/invitable_friends?access_token={userAccessToken}
&limit={numFriendsToRetrieve}
$after={pagingAfterToken}'

userId is stored in rethinkDB under id value in people.Facebook
userAccessToken is stored in rethinkDB under token value in people.Facebook
numFriendsToRetrieve is an integer with default value of 25
but can be set to be as high as 100, will use 100 to make at few API calls as possible
pagingAfterToken is returned in the paging object given by the graph API call; we cannot hard code this before hand
 */
module.exports = function(userId,userAccessToken,afterToken,callback) {
        if (afterToken) {
            get_all_my_facebook_friends(
                userId,userAccessToken,afterToken,
                function (all_my_friends) {
                        console.log('All my facebook friends are ');
                        console.log(all_my_friends.length);
                        return callback(all_my_friends);
                    }
            );
        }
    }
    function get_all_my_facebook_friends(userId,userAccessToken,afterToken,callback, friends_so_far ) {
        friends_so_far = friends_so_far || [];
        var url = "https://graph.facebook.com/v2.4/" + userId + "/invitable_friends?access_token="
                + userAccessToken + "&limit=100&after=" + afterToken;
        // we are going to call this recursively, appending the users as we go
        // until facebook decides we don't have anymore friends ( so this shouldn't take long )
        // then we pass that list of friends to the callback

        console.log('fetching... ' + url);
        request(
            url,
            function ( err, resp, body ) {
                var body = JSON.parse(body),
                    data = body.data;
                if ( err || body.error ) {
                    callback( err || body.error );
                } else {
                    if ( data.length > 0 ) {
                        var nextAfterToken;
                        // something was returned... keep on keepin on
                        while ( next = data.pop() ) {
                            friends_so_far.push( next );
                        }
                        nextAfterToken=body.paging.cursors.after;
                        console.log('======>');
                        console.log(nextAfterToken);
                        get_all_my_facebook_friends(userId,userAccessToken,nextAfterToken, callback, friends_so_far );
                    } else {
                        // we're done! call the callback!
                        callback(friends_so_far);
                    }
                }
            }
        );

    }



