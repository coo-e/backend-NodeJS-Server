// app/routes.js
var thinky = require('thinky')({
    host: '45.55.60.150',
    port: 28015,
    authKey: '',
    db: 'people'
});
var chatController = require('../controller/chatController.js');
var Activities  = require('../app/models/users').Activities;
var Google = require('../app/models/users').Google;
var request =require('superagent');
var r = thinky.r;
module.exports = function(app, passport) {

    // =====================================
    // HOME PAGE (with login links) ========
    // =====================================

    app.get('/', function(req, res, next) {
        res.render('fixedHeader.ejs', { title: 'njit stuff' });
    });
    // =====================================
    // LOGIN ===============================
    // =====================================
    // show the login form
    app.get('/login', function(req, res) {

        // render the page and pass in any flash data if it exists
        res.render('fixedHeader.ejs', { title: 'njit stuff' });
    });

    // process the login form
    // app.post('/login', do all our passport stuff here);

    // =====================================
    // SIGNUP TEMPORARILY DISABLED==========
    // =====================================
    // show the signup form
    //app.get('/signup', function(req, res) {
    //
    //    // render the page and pass in any flash data if it exists
    //    res.render('signup.ejs', { message: req.flash('signupMessage') });
    //});

    // process the signup form
    //app.post('/signup', //do all our passport stuff here

    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect : '/analytics', // redirect to the secure profile section
        failureRedirect : '/signup', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));

    app.get('/angular2',function(req,res){
       res.render('angular2.jade')
    });

    // =====================================
    // PROTECTED ROUTES ====================
    // =====================================
    // we will want this protected so you have to be logged in to visit
    // we will use route middleware to verify this (the isLoggedIn function)
    app.get('/createActivity',isLoggedIn,function(req,res,next){
        res.render('createActivity.ejs',
            {title:'create activity',
                user:req.user// get the user out of session and pass to template
            })
    });
    app.get('/chatTest',isLoggedIn,function(req,res,next){
        res.render('rabbitChat.jade',{title:'chat',
            user:req.user,// get the user out of session and pass to template
            chatExchange:req.query.exchange
             });
        console.log(req.query.exchange)
    });



    app.post('/publishMessage', chatController.publishMessage);

    app.post('/createActivity',isLoggedIn,function(req,res,next){
        var exchangeName = req.body.activity +'@'+req.body.exchangeName;
        var messageBody = req.body.activity;
        var newActivity = new Activities({
            activity:messageBody,
            exchangeName:exchangeName
        });
        Activities.getAll([messageBody, exchangeName],{index: "activity@exchange"}).run().then(
            function(activityArray){
                var activity = activityArray[0];
                if(activity){
                    console.log("activity: " +activity+ "already in DB");
                    return next(JSON.stringify(activity) + "already in DB and rabbitmq");
                }else{
                    newActivity.save().then(function (newActivity) {
                        request
                            .post('http://localhost:3000/publishMessage')
                            .send(newActivity)
                            .end(function(err,res){
                                if(err || !res.ok){
                                    console.error('there was an error: ' + err);
                                    console.log(newActivity);
                                }else{
                                    //var newNote = {
                                    //    date: r.now(),
                                    //    from: "Inigo Montoya",
                                    //    subject: "You killed my father"
                                    //};
                                    //r.table("users").get(10001).update(
                                    //    {notes: r.row("notes").append(newNote)}
                                    //).run(conn, callback)
                                    var newUserActivity = {
                                        activity:newActivity.activity,
                                        exchangeName:newActivity.exchangeName
                                    };
                                    Google.getAll(req.user.id,{index:'id'}).update({
                                        activities: r.row("activities").append(newUserActivity)
                                    }).run().then(function(newUserActivityAdded){
                                        console.log("published to amqp: ");
                                        return next(JSON.stringify(newUserActivityAdded) + "saved to amqp and user profile")
                                    })

                                }
                            })
                    });
                }
            }
        )

    });


    //app.get('/profile', isLoggedIn, function(req, res) {
    //    res.render('profile.ejs', {
    //        user : req.user // get user out of session and pass to template
    //    });
    //});

    // =====================================
    // GOOGLE ROUTES =====================
    // =====================================
    // route for le goog authentication and login
    app.get('/auth/google', passport.authenticate('google', { scope : ['email','profile']}));

    app.get('/auth/google/callback',
        passport.authenticate('google', {
            successRedirect : '/chatTest?exchange=soccer@newark',
            failureRedirect : '/fixed'
        })
    );
    // =====================================
    // FACEBOOK ROUTES =====================
    // =====================================
    // route for facebook authentication and login
    app.get('/auth/facebook', passport.authenticate('facebook', { scope : ['email' ,'public_profile']}));

    // handle the callback after facebook has authenticated the user
    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {scope:['email','public_profile'],
            successRedirect : '/createActivity',
            failureRedirect : '/fixed'
        }));


    // =====================================
    // LOGOUT ==============================
    // =====================================
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

    // =====================================
    // LOGIN ===============================
    // =====================================
    // process the login form
    app.post('/api/users', passport.authenticate('local-login', {
        successRedirect : '/analytics', // redirect to the secure profile section
        failureRedirect : '/createActivity', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));

};


// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/login');
}
//
//function isAdmin(req,res,next){
//
//}