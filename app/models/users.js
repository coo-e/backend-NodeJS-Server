var thinky = require('thinky')({
    host: '45.55.60.150',
    port: 28015,
    authKey: '',
    db: 'people'
});
var type = thinky.type;
var bcrypt   = require('bcrypt-nodejs');

var stringify = function(doc) {
    return JSON.stringify(doc, null, 2);
};
exports.Users = thinky.createModel( "User",{
    emailUser:type.string(),
    password:type.string(),
    admin:type.boolean().default(false),
    facebook:{
        id:type.string(),
        token:type.string(),
        email:type.string(),
        photo:type.string(),
        name:type.string()
    },
    google:{
        id:type.string(),
        email:type.string(),
        token: type.string(),
        name: type.string(),
        photo:type.string()
    }
});

exports.Google = thinky.createModel(
    "Google",{
        id:type.string(),
        email:type.string(),
        name: type.string(),
        photo:type.string(),
        activities:[{activity:type.string().default('soccer'),
            exchangeName:type.string().default('soccer@newark')}]
    }
);

exports.Activities = thinky.createModel("Activities",{
    activity:type.string(),
    exchangeName:type.string()
});

exports.Activities.changes().then(function(feed){
    feed.each(function(error, doc) {
        if (error) {
            console.log(error);
            process.exit(1);
        }
        if (doc.isSaved() === false) {
            console.log("The following document was deleted:");
            console.log(stringify(doc));
        }
        else if (doc.getOldValue() == null) {
            console.log("A new document was inserted:");
            console.log(stringify(doc));
        }
        else {
            console.log("A document was updated.");
            console.log("Old value:");
            console.log(stringify(doc.getOldValue()));
            console.log("New value:");
            console.log(stringify(doc));
        }
    });
}).error(function(error) {
    console.log(error);
    process.exit(1);
});
//exports.Activities.hasAndBelongsToMany(exports.Google,"activities","id","id");
//exports.Google.hasAndBelongsToMany(exports.Activities,"activities","id","id");
exports.Facebook = thinky.createModel("Facebook",{
    id:type.string(),
    token:type.string(),
    email:type.string(),
    photo:type.string(),
    name:type.string()
});

exports.Facebook.belongsTo(exports.Users, "User","email","emailUser");
exports.Google.belongsTo(exports.Users, "User","email","emailUser");

exports.Users.comparePassword = function(password, user, callback) {
    bcrypt.compare(password, user.password, function(err, match) {
        if (err) callback(err);
        if (match) {
            callback(null, true);
        } else {
            callback(err);
        }
    });
};


