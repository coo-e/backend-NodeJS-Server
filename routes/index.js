var express = require('express');
var router = express.Router();
var Handlebars = require('handlebars');


Handlebars.registerHelper('table',function(waitTimeByHour){

});
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('fixedHeader.ejs', { title: 'tellUsWho' });
});

router.get('/carousel', function(req,res,next) {
	res.render('carousel.jade',{title :'tellUsWho'});
});

router.get('/fixed',function(req,res,next){
   res.render('fixedHeader.ejs',{title:'tellUsWho'});
});


router.get('/react',function(req,res,next){
    res.render('reactTest.jade',{title:'React Test',
        chartTitle:'H1b Salaries'})
});


module.exports = router;
