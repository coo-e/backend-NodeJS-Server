var User    = require('../app/models/users').Users;
var express = require('express');
var router  = express.Router({
    mergeParams: true
});
var r = require('rethinkdb');


var chatController = require('../controller/chatController.js');




module.exports = function(passport) {
    //USER LOGGED IN CHECK ! ======================================================================
    function isLoggedIn(req, res, next){
        if(req.isAuthenticated()){
            return next();
        }
        res.redirect('/');
    };

    router.post('/publishMessage', chatController.publishMessage);
    //USER PROFILE ================================================================================
    router.get( '/profile', isLoggedIn, function (req, res) {
        r.table('matchdata')
            .filter({id: req.user.id})
            .run(req._rdbConn, function(error, cursor) {
                if (error) {
                    handleError(res, error);
                    next();
                }
                else {
                    // Retrieve all the results
                    cursor.toArray(function(error, result) {
                        if (error) {
                            handleError(res, error);
                        }
                        else {
                            // Send back the data as json
                            res.send(JSON.stringify(result));
                        }
                    });
                }
            });
    });


    return router;
}
