/**
 * Created by kingHenry on 1/4/16.
 */
// config/passport.js

// load all the things we need
var LocalStrategy   = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
// load up the user model
var User            = require('../app/models/users').Users;
var Facebook = require('../app/models/users').Facebook;
var Google = require('../app/models/users').Google;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var bcrypt=require('bcrypt-nodejs');
var configAuth = require('./auth');
var R = require('ramda');
var facebookFriends = require('../app/facebookFriends');

// expose this function to our app using module.exports
module.exports = function(passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function (id, done) {
        Google.get(id)
            .run()
            .then(function(user) {
                done(null, user);
            });
    });
    // =========================================================================
    // GOOGLE ==================================================================
    // =========================================================================
    passport.use(new GoogleStrategy({

            clientID        : configAuth.googleAuth.clientID,
            clientSecret    : configAuth.googleAuth.clientSecret,
            callbackURL     : configAuth.googleAuth.callbackURL,
            profileFields: ['email','profile']

        },
        function(token, refreshToken, profile, done) {

            // make the code asynchronous
            // User.findOne won't fire until we have all our data back from Google
            process.nextTick(function() {

                // try to find the user based on their google id
                Google.getAll(profile.id,{ index:'id'}).run().then(function(userArray) {
                    var user = userArray[0];
                    if (user) {

                        // if a user is found, log them in
                        return done(null, user);
                    } else {
                        var rawProfile =JSON.parse(profile._raw);
                        console.log(rawProfile);
                        // if the user isnt in our database, create a new user
                        var newUser          = new Google({
                            id: rawProfile.id,
                            email: rawProfile.emails[0].value,
                            photo: rawProfile.image.url,
                            name: rawProfile.displayName,
                            activities: [{activity:'sample',exchangeName:'none'}]
                        });

                        // save the user
                        newUser.save().then(function (userInfo) {
                            return done(null, newUser);
                        });
                    }
            });
        });
}));

    // =========================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    passport.use(new FacebookStrategy({
            // pull in our app id and secret from our auth.js file
            clientID        : configAuth.facebookAuth.clientID,
            clientSecret    : configAuth.facebookAuth.clientSecret,
            callbackURL     : configAuth.facebookAuth.callbackURL,
            profileFields: ['id', 'displayName', 'photos','email','friends','invitable_friends']
        },
        // facebook will send back the token and profile
        function(token, refreshToken, profile, done) {
            // asynchronous
            process.nextTick(function() {
                // find the user in the database based on their facebook id
                Facebook.getAll(profile.id,{ index:'id'}).run().then(function(userArray) {
                    // if the user  found, then log them in
                    var user = userArray[0];
                    if (user) {
                        //var friendPicUrl=invitableFriends.data.picture.data.url;
                        return done(null, user); // user found, return that user
                    } else {
                        var rawProfile =JSON.parse(profile._raw);
                        //facebookFriends(profile.id,token,afterToken,function(friends){
                                // if there is no user found with that facebook id, create them
                                var newUser = new Facebook({
                                    id: profile.id,
                                    token: token,
                                    email: profile.emails[0].value,
                                    photo: profile.photos[0].value,
                                    name: profile.displayName
                                    //contacts: friends
                                });
                                // save our user to the database
                                newUser.save().then(function (userInfo) {
                                    return done(null, newUser);
                                });
                            //})
                    }
                });
            });
        }));
    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'
    passport.use('local-signup', new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField : 'email',
            passwordField : 'password',
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, email, password, done) {
            var emailField = email;
            //var password = bcrypt.hashSync(password);
            process.nextTick(function() {
                // find a user whose email is the same as the forms email
                // we are checking to see if the user trying to login already exists
                User.getAll(emailField, {index: 'emailUser'}).run().then(function(err, userArray) {
                    // if there are any errors, return the error
                    console.log(emailField);
                    //var user = userArray[0];
                    // check to see if theres already a user with that email
                    if (userArray) {
                        return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
                    } else {
                        console.log('saving user...');
                        // if there is no user with that email
                        // create the user
                        var newUser = new User({
                            emailUser:emailField,
                            password:password,
                            admin:false
                        });
                        newUser.save().then(function(userInfo){
                            console.log(userInfo);
                            return done(null,newUser);
                        })
                    }

                });

            });
        })),
        // =========================================================================
        // LOCAL Log-In ============================================================
        // =========================================================================
        // we are using named strategies since we have one for login and one for signup
        // by default, if there was no name, it would just be called 'local'
        passport.use('local-login', new LocalStrategy({
                // by default, local strategy uses username and password, we will override with email
                usernameField: 'email',
                passwordField: 'password',
                passReqToCallback: true // allows us to pass back the entire request to the callback
            },
            function (req, email, password, done) {
                var emailField = email;
                var password = password;
                // asynchronous
                // User.findOne wont fire unless data is sent back
                process.nextTick(function () {
                    // find a user whose email is the same as the forms email
                    // we are checking to see if the user trying to login already exists
                    User.getAll(emailField, {index: 'emailUser'}).run().then(function (userArray) {
                        // check to see if theres already a user with that email
                        var user = userArray[0];
                        console.log(emailField);
                        console.log(userArray);
                        if (!user) {
                            return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash
                            console.log('no user found');
                            console.log(email);
                        }
                        User.comparePassword(password, user, function (err, valid) {
                            console.log(user);
                            if (err) {
                                return done(null, false, req.flash('loginMessage', err));
                                console.log(err);
                            }
                            if (!valid) {
                                return done(null, false, req.flash('loginMessage', 'The username or password is incorrect!'));
                                console.log('username or password incorrect!');
                            } else {
                                console.log("logged in!!");
                                return done(null,user)
                            }
                        })


                    })
                })
            }))
};







