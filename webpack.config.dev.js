var path = require('path');
var webpack = require('webpack');

module.exports = {
    devtool: 'eval',
    entry: {
        'react':'./src/main',
        'angular2':'./src/ng-boot.ts',
        'ChatRoom':'./src/logic/listener-app'
    },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].bundle.js',
        publicPath: '/static/',
        library: 'ChatRoom',
        libraryTarget: 'umd'
    },
    plugins: [
        //new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin()
    ],
    resolve: {
      extensions: ['','.js','.jsx']
    },
    module: {
        loaders: [
        {
            test: /\.js|\.jsx$/,
            loaders: ['babel'],
            include: path.join(__dirname, 'src')
        }, {
            test: /\.ts/,
            loaders: ['ts-loader'],
            include: path.join(__dirname, 'src'),
            exclude: /node_modules/
        },
        {
            test: /\.less$/,
            loaders: ["style!css!less"],
            include: path.join(__dirname, 'src')
        }
    ]
    }
};